## Wanderluster
*Wanderluster* is an application that allows you to **add a travel plan** and its modifications. 
In addition, it allows users to **view plans and filter results** using a search engine. Additional functionalities, 
such as the **Favorites tab** or **Shelf tab**, combine the functionality of competing applications on the market, but present 
them in a different form. 

### More info
Please refer to [this README file](https://bitbucket.org/kamilapalonek/trip-it-frontend/src/master/README.md) to get information about the project.