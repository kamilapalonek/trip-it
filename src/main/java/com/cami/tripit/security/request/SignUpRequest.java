package com.cami.tripit.security.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;

@Getter
@Setter
public class SignUpRequest {
    private String name;
    private String username;
    @Email
    private String email;
    private String password;
}