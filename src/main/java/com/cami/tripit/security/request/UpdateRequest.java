package com.cami.tripit.security.request;

import com.cami.tripit.model.user.UserInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateRequest extends SignUpRequest {
    UserInfo userInfo;

}
