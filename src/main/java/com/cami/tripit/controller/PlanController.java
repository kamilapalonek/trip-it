package com.cami.tripit.controller;

import com.cami.tripit.model.plan.Plan;
import com.cami.tripit.payload.request.FilterPlanRequest;
import com.cami.tripit.service.FavoritesService;
import com.cami.tripit.service.PlanService;
import com.cami.tripit.service.ShelfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/plans")
public class PlanController {
    @Autowired
    private PlanService planService;
    @Autowired
    private FavoritesService favoritesService;
    @Autowired
    private ShelfService shelfService;

    @GetMapping
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok().body(planService.findAll());
    }

    //TODO: responseentity refactor

    @GetMapping("/plan")
    public ResponseEntity<?> getOne(@RequestParam String id) {
        //TODO: service to return plan, not optional
        Optional<Plan> plan = planService.findById(id);
        return plan.map(val -> ResponseEntity.ok().body(val)).orElseThrow();
    }

    @PostMapping("/plan/faves")
    public ResponseEntity<?> getFave(@RequestParam String planId, @RequestParam String username) {
        return ResponseEntity.ok().body(favoritesService.getFaves(planId, username));
    }

    @PostMapping("/plan")
    public ResponseEntity<?> addOrUpdatePlan(@ModelAttribute Plan plan, @RequestParam String userId) {
        planService.addOrUpdatePlan(plan, userId);
        return ResponseEntity.ok().body("Plan updated succcessfully");
    }

    @DeleteMapping
    public void deletePlan(@RequestParam String id) {
        planService.deletePlan(id);
    }

    @GetMapping("/user")
    public ResponseEntity<?> getAllPlansByUsername(@RequestParam String username) {
        return ResponseEntity.ok().body(planService.getAllPlansByUsername(username));
    }

    //TODO: refactor
    @GetMapping("/faves")
    public ResponseEntity<?> getFaves() {
        return ResponseEntity.ok().body(planService.findTop10ByOrderByFaves());
    }

    @GetMapping("/faved")
    public ResponseEntity<?> findFaves(@RequestParam String by) {
        return ResponseEntity.ok().body(favoritesService.findFaves(by));
    }

    @PostMapping("/shelf")
    public ResponseEntity<?> addOrUpdateShelf(@RequestParam String username, @RequestParam String folderName) {
        shelfService.addOrUpdateShelf(username, folderName);
        return ResponseEntity.ok().body("Upated succefully!");
    }


    //TODO: to object
    @GetMapping("/search")
    public ResponseEntity<?> getMatching(@Valid FilterPlanRequest planRequest) {
        return ResponseEntity.ok().body(planService.getMatching(planRequest));
    }
};