package com.cami.tripit.controller;

import com.cami.tripit.model.user.User;
import com.cami.tripit.security.config.CurrentUser;
import com.cami.tripit.security.model.UserPrincipal;
import com.cami.tripit.security.model.UserSummary;
import com.cami.tripit.security.request.UpdateRequest;
import com.cami.tripit.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/me")
    public UserSummary getCurrentUser(@CurrentUser UserPrincipal currentUser) {
        return new UserSummary(currentUser.getId(), currentUser.getUsername(), currentUser.getName());
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable String id) {
        userService.deleteUser(id);
    }

    @GetMapping("/{username}")
    public ResponseEntity<?> findUserByUsername(@PathVariable String username) {
        //TODO: service o return object not optional
        Optional<User> user = userService.findByUsername(username);
        return user.map(val -> ResponseEntity.ok().body(val)).orElseThrow();
    }

    @GetMapping
    public ResponseEntity<?> findUser(@RequestParam String id) {
        //TODO: service o return object not optional
        Optional<User> user = userService.findUser(id);
        return user.map(val -> ResponseEntity.ok().body(val)).orElseThrow();
    }

    @PostMapping
    public ResponseEntity<?> addOrUpdateUser(@RequestParam String id, @Valid @ModelAttribute UpdateRequest updateRequest) {
        userService.addOrUpdateUser(id, updateRequest);
        return ResponseEntity.ok().body("User updated succcessfully");
    }
}
