package com.cami.tripit.repository;

import com.cami.tripit.model.plan.Plan;
import com.cami.tripit.model.user.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface PlanRepository extends MongoRepository<Plan, String> {
    List<Plan> findAll();

    Optional<Plan> findById(String id);

    List<Plan> findPlansByAuthor(User user);

    //TODO: findAllByAuthorUsername

    List<Plan> findTop10ByOrderByFavesDesc();

    List<Plan> findAllByCosts(String costs);

    List<Plan> findAllByDaysSpent(int daysSpent);

}
