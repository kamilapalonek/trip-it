package com.cami.tripit.service;

import com.cami.tripit.model.plan.Plan;
import com.cami.tripit.model.user.User;
import com.cami.tripit.payload.request.FilterPlanRequest;
import com.cami.tripit.repository.PlanRepository;
import com.cami.tripit.security.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PlanServiceImpl implements PlanService {
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("MM/dd/yyyy");

    @Autowired
    private PlanRepository planRepository;
    @Autowired
    private UserService userService;

    @Override
    public List<Plan> findAll() {
        return planRepository.findAll();
    }

    @Override
    public void saveOrUpdatePlan(Plan plan) {
        planRepository.save(plan);
    }

    @Override
    public void deletePlan(String id) {
        planRepository.deleteById(id);
    }

    @Override
    public List<Plan> findPlansByAuthor(User user) {
        return planRepository.findPlansByAuthor(user);
    }

    @Override
    public Optional<Plan> findById(String id) {
        return planRepository.findById(id);
    }

    @Override
    public List<Plan> findTop10ByOrderByFaves() {
        return planRepository.findTop10ByOrderByFavesDesc();
    }

    @Override
    public List<Plan> findAllByCosts(String costs) {
        return planRepository.findAllByCosts(costs);
    }

    @Override
    public List<Plan> findAllByDaysSpent(int daysSpent) {
        return planRepository.findAllByDaysSpent(daysSpent);
    }

    @Override
    public void addOrUpdatePlan(Plan plan, String userId) {

        LocalDate startDate = getFormattedDate(plan.getStartDate());
        LocalDate endDate = getFormattedDate(plan.getEndDate());
        int daysSpent = Period.between(startDate, endDate).getDays();

        plan.setDaysSpent(daysSpent);
        plan.setCreatedAt(LocalDate.now().format(FORMATTER));
        plan.setPlanType(plan.getPlanType());
        
        Optional<User> user = userService.findUser(userId);
        user.ifPresentOrElse(plan::setAuthor, () -> new ResourceNotFoundException("User can't be found"));

        saveOrUpdatePlan(plan);
    }

    //TODO: define filter class
    @Override
    public List<Plan> getMatching(final FilterPlanRequest planRequest) {
        return findAll().stream()
                .filter(plan -> filterOutLocation(plan, planRequest.getLocation()))
                .filter(plan -> filterOutDays(plan, planRequest.getDays()))
                .filter(plan -> filterOutBudget(plan, planRequest.getBudgetFrom(), planRequest.getBudgetTo()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Plan> getAllPlansByUsername(String username) {
        User user = userService.findByUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException("Not found!"));

        return findPlansByAuthor(user);
    }

    private LocalDate getFormattedDate(String date) {
        return LocalDate.parse(date, FORMATTER);
    }

    private boolean filterOutBudget(Plan plan, int budgetFrom, int budgetTo) {
        String budget = plan.getCosts();
        int costs = !budget.isBlank() ? Integer.valueOf(budget) : 0;

        return budgetFrom <= costs && costs <= budgetTo;
    }

    private boolean filterOutLocation(Plan plan, String location) {
        return plan.getDailyPlan().stream()
                .anyMatch(p -> p.getLocation().contains(location));
    }

    private boolean filterOutDays(Plan plan, List<Integer> days) {
        int from = days.get(0);
        int to = days.get(1);
        int daysSpent = plan.getDaysSpent();

        return from <= daysSpent && daysSpent <= to;
    }

}
