package com.cami.tripit.service;

import com.cami.tripit.model.plan.Plan;
import com.cami.tripit.model.user.User;
import com.cami.tripit.payload.request.FilterPlanRequest;

import java.util.List;
import java.util.Optional;

public interface PlanService {
    List<Plan> findAll();

    Optional<Plan> findById(String id);

    void saveOrUpdatePlan(Plan plan);

    void deletePlan(String id);

    List<Plan> findPlansByAuthor(User user);

    List<Plan> findTop10ByOrderByFaves();

    List<Plan> findAllByCosts(String costs);

    List<Plan> findAllByDaysSpent(int daysSpent);

    void addOrUpdatePlan(Plan plan, String userId);

    List<Plan> getMatching(FilterPlanRequest planRequest);

    List<Plan> getAllPlansByUsername(String username);


}
