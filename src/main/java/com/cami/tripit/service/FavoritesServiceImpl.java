package com.cami.tripit.service;

import com.cami.tripit.model.plan.Plan;
import com.cami.tripit.model.user.User;
import com.cami.tripit.security.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FavoritesServiceImpl implements FavoritesService {
    @Autowired
    private UserService userService;
    @Autowired
    private PlanService planService;

    @Override
    public int getFaves(String planId, String username) {
        User user = userService.findByUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException("Not found!"));
        Plan plan = planService.findById(planId)
                .orElseThrow(() -> new ResourceNotFoundException("Not found!"));

        List<User> usersFaved = plan.getUsersFaved();
        int faves = plan.getFaves();
        if (usersFaved.contains(user)) {
            usersFaved.remove(user);
            plan.setFaves(faves - 1);
        } else {
            usersFaved.add(user);
            plan.setFaves(faves + 1);
        }

        planService.saveOrUpdatePlan(plan);
        return plan.getFaves();
    }

    @Override
    public List<Plan> findFaves(String by) {
        User user = userService.findByUsername(by)
                .orElseThrow(() -> new ResourceNotFoundException("Not found!"));

        return planService.findAll().stream()
                .filter(plan -> plan.getUsersFaved().contains(user))
                .collect(Collectors.toList());

    }
}
