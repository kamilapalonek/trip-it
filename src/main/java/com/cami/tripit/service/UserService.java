package com.cami.tripit.service;

import com.cami.tripit.model.user.User;
import com.cami.tripit.security.request.UpdateRequest;

import java.util.Optional;

public interface UserService {
    Optional<User> findUser(String id);

    void deleteUser(String id);

    void saveOrUpdateUser(User user);

    void addOrUpdateUser(String id, UpdateRequest updateRequest);

    Optional<User> findByUsername(String username);


}
