package com.cami.tripit.service;

import com.cami.tripit.model.user.User;
import com.cami.tripit.repository.UserRepository;
import com.cami.tripit.security.exception.ResourceNotFoundException;
import com.cami.tripit.security.request.UpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.validation.Valid;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void saveOrUpdateUser(User user) {

        userRepository.save(user);
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Optional<User> findUser(String id) {
        return userRepository.findById(id);
    }

    @Override
    public void deleteUser(String id) {
        userRepository.deleteById(id);
    }

    @Override
    public void addOrUpdateUser(String id, UpdateRequest updateRequest) {
        User user = findUser(id)
                .orElseThrow(() -> new ResourceNotFoundException("Id not found"));

        user.setName(updateRequest.getName());
        user.setEmail(updateRequest.getEmail());
        setNewPasswordOrUseOld(updateRequest, user);
        user.setUserInfo(updateRequest.getUserInfo());

        saveOrUpdateUser(user);
    }

    private void setNewPasswordOrUseOld(@ModelAttribute @Valid UpdateRequest updateRequest, User user) {
        String password = updateRequest.getPassword();

        if (password.equals("")) {
            user.setPassword(user.getPassword());
        } else {
            user.setPassword(password);
        }
    }
}
