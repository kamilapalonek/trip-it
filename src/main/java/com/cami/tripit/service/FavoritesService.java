package com.cami.tripit.service;

import com.cami.tripit.model.plan.Plan;

import java.util.List;

public interface FavoritesService {
    int getFaves(String planId, String username);

    List<Plan> findFaves(String by);
}
