package com.cami.tripit.payload.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FilterPlanRequest {
    String location;
    List<Integer> days;
    int budgetFrom;
    int budgetTo;
}
