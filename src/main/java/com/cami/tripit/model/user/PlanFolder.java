package com.cami.tripit.model.user;

import lombok.*;

import java.util.List;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PlanFolder {
    String folderName;
    Map<BookmarkedPlace, List<String>> places;

    @Data
    private class BookmarkedPlace {
        private String tripId;
        private String tripLocation;
    }
}
