package com.cami.tripit.model.plan;

import com.cami.tripit.model.user.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document
@AllArgsConstructor
@Getter
@Setter
public class Plan {
    @Id
    private String id;
    private String title;
    private String coverPhoto;
    private String createdAt;
    List<Day> dailyPlan;
    private String startDate;
    private String endDate;
    private int daysSpent;
    private String costs;
    @DBRef
    private User author;
    private int faves;
    PlanInfo planInfo;
    List<User> usersFaved;
    String planType;

    public Plan() {
        usersFaved = new ArrayList<>();
    }
}