package com.cami.tripit.model.plan;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PlanInfo {
    private String mustHave;
    private String mustSkip;
    private String otherComments;
}